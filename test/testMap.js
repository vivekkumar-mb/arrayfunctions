const map = require("../map");




const items = [1, 2, 3, 4, 5, 5];


const greaterthan2 = map(items, (item, i, arr) => {
    console.log(item, i, arr);
    return item > 2 ? item : 0;
    //only returning items if they are greater than 2 else returning 0
});
console.log(greaterthan2);

const onePlusarr = map(items, (item, i, arr) => {
    console.log(item, i), arr;
    return ++item;
    //incrementing the values by one
});

console.log(onePlusarr);