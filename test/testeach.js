const each = require('../each.js');

const items = [1, 2, 3, 4, 5, 5];



each(items, (item, i,arr) => {
    console.log(item, i,arr); //printing each item
});


each(items, (item, i,arr) => {
    item += 1;
    console.log(item, i,arr); //incrementing 1 in each item and printing the values
});