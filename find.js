
const find = (arr, cb) => {

    for (let i = 0; i < arr.length; i++) {

        if(cb(arr[i], find,arr)) {
            return (arr[i]);
        }
    }
    return undefined;

}

module.exports = find;