function each(arr, cb) {
    if (arr.length == 0) return;
    for (let i = 0; i < arr.length; i++) {
        cb(arr[i], i,arr);

    }
}

module.exports = each;