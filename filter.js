const filter = (arr, cb) => {
    let temp = []
    for (let i = 0; i < arr.length; i++) {
        if (cb(arr[i],i,arr)) {
            temp.push(arr[i]);
        }


    }
    return temp;
}
module.exports = filter;