const flatten = (arr) => {

  if (!arr || !Array.isArray(arr)) return [];

  const masterarr = [];

  for (let i = 0; i < arr.length; i++) {
    if (Array.isArray(arr[i])) {
      masterarr.push(...flatten(arr[i]));
    } else {
      masterarr.push(arr[i]);
    }
  }

  return masterarr;
};
module.exports = flatten;