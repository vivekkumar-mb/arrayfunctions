const map = (arr, cb) => {
    let temp = [];
    for (let i = 0; i < arr.length; i++) {
        temp.push(cb(arr[i], i, arr));
    }

    return temp;
}

module.exports = map;