const reduce = (items, cb, startingValue) => {
    let accumulatorpassed = false;
    let currentvale;
    if (!startingValue) {
        startingValue = items[0];
        accumulatorpassed = true;
    }
    for (let i = 0; i < items.length; i++) {
        if (accumulatorpassed) {
            accumulatorpassed = false;
            continue;
        }
        currentvale = cb(startingValue, items[i],i,items);
        startingValue = currentvale;
    }

    return currentvale;

}
module.exports = reduce;